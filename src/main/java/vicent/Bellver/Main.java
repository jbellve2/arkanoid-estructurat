package vicent.Bellver;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Main {

    //<editor-fold desc="Variables Estàtiques">
    static int maxF = 25;
    static int maxC = 15;
    static int taulell[][] = new int[maxF][maxC];

    static int velocitat = 200;
    static boolean pausa=false;
    static int filaPilota = maxF - 3;
    static int colPilota = maxC / 2;
    static int posNau = maxC / 2;
    static int direccioPilota = 9;               // -> diagonals 7, 9, 1, 3
    static boolean fiJoc = false;
    static int contadorTemps = 0;
    static int contaPeces;
    static int puntsTotals = 0;
    static boolean punt = false;

    static String blauC = "\033[3;34m";             // per colorejar de blau
    static String verdN = "\033[1;32m";             // per colorejar de verd
    static String roigN = "\033[1;31m";             // per colorejar de roig
    static String cafeN = "\033[1;33m";             // per colorejar de café
    static String purpuraN = "\033[1;35m";          // per colorejar de púrpura
    static String nc = "\033[0m";                   // per retornar al color normal

    static int maxFantasmes = 3;
    static int[][] fantasmes = new int[maxFantasmes][3];   // fantasmes, posició i direcció
    static int[] dirsFantasmes = {1, 2, 3, 6, 9, 8, 7, 4};
    static char fantasma = '&';
    // i -> número de fantasmes
    // j -> (0) - posició X (i)   (1) - posició Y (j)   (2) - direcció (2, 4, 6, 8)


    static String sistemaOperatiu = System.getProperty("os.name");
    static Random rnd = new Random();

    //</editor-fold>


    public static void main(String[] args) throws InterruptedException {



        /**
         * CODIFICACIÓ:
         *
         * -1   -> pilota
         * 0    -> blanc
         * 1    -> paret
         * 2    -> peça de color
         * 3    -> nau movible arkanoid
         *
         *
         */


        //<editor-fold desc="Codi Event Botó">
        Frame f = new Frame("Taulell de control");
        f.setLayout(new FlowLayout());
        f.setSize(500, 100);        // TAMAY DEL FRAME
        Label l = new Label();
        l.setText("Posa ací el ratolí i prem botons dreta o esquera del teclat");
        f.add(l);
        f.setVisible(true);


        // POSEM EL CODI DEL LISTENER (cada cop que premem algun botó)
        KeyListener listener = new KeyListener() {
            @Override
            public void keyPressed(KeyEvent event) {
//                if (espai[maxF - 1][llocNau] == -1) {
//
//                }
                if (event.getKeyCode() == KeyEvent.VK_RIGHT) {      // al prèmer tecla dreta
                    if (taulell[maxF - 2][posNau + 1] != 1) {
                        taulell[maxF - 2][posNau] = 0;
                        posNau++;
                        taulell[maxF - 2][posNau] = 1;
                    }

                }
                if (event.getKeyCode() == KeyEvent.VK_LEFT) {       // al prèmer la tecla esquerra
                    if (taulell[maxF - 2][posNau - 1] != 1) {
                        taulell[maxF - 2][posNau] = 0;
                        posNau--;
                        taulell[maxF - 2][posNau] = 1;
                    }
                }
                if (event.getKeyCode() == KeyEvent.VK_UP) {       // al prèmer la tecla AMUNT

                }
                if (event.getKeyCode() == KeyEvent.VK_DOWN) {       // al prèmer la tecla AVALL

                }
                if (event.getKeyCode() == KeyEvent.VK_P) {      // al prèmer tecla dreta
                    pausa=true;

                }
                if (event.getKeyCode() == KeyEvent.VK_R) {      // al prèmer tecla dreta
                    pausa=false;

                }
            }

            @Override
            public void keyReleased(KeyEvent event) {
                //       printEventInfo("Key Released", event);
            }

            @Override
            public void keyTyped(KeyEvent event) {
                //      printEventInfo("Key Typed", event);
            }

            private void printEventInfo(String str, KeyEvent e) {
//                System.out.println(str);
//                int code = e.getKeyCode();
//                System.out.println("   Code: " + KeyEvent.getKeyText(code));
//                System.out.println("   Char: " + e.getKeyChar());
//                int mods = e.getModifiersEx();
//                System.out.println("    Mods: "
//                        + KeyEvent.getModifiersExText(mods));
//                System.out.println("    Location: "
//                        + keyboardLocation(e.getKeyLocation()));
//                System.out.println("    Action? " + e.isActionKey());
            }

            private String keyboardLocation(int keybrd) throws InterruptedException {
                switch (keybrd) {
                    case KeyEvent.KEY_LOCATION_RIGHT:

                        // return "Right";

                    case KeyEvent.KEY_LOCATION_LEFT:
                        // canviaPos(false);
                        //   return "Left";

                    case KeyEvent.KEY_LOCATION_NUMPAD:
                        return "NumPad";
                    case KeyEvent.KEY_LOCATION_STANDARD:
                        return "Standard";
                    case KeyEvent.KEY_LOCATION_UNKNOWN:
                    default:
                        return "Unknown";
                }
            }
        };

        // AFEGIM EL listener AL JPANELL CREAT DALT
        f.addKeyListener(listener);
        //</editor-fold>


        //<editor-fold desc="Inici matriu i posició base inicial">
        // inicialitzem la matriu a 0's i posem ja les parets i les fitxes on toquen
        if (sistemaOperatiu.contains("indows")) {
            //  blauC = roigN = cafeN = purpuraN = verdN = nc = "";          // tristament windows no té colors a la terminal
            System.out.println("DETECTAT SISTEMA OPERATIU WINDOWS");
            System.out.println(" NO FUNCIONARÀ AMB cmd NI POWER SHELL");
            System.out.println("INSTAL·LA LA CONSOLA git bash I EXECUTA DES D'ALLÀ");
            Thread.sleep(7000);

        }


        // INICIALITZACIÓ PARETS I PECES DE COLOR A LA MATRIU
        for (int i = 0; i < maxF; i++) {
            for (int j = 0; j < maxC; j++) {
                if (i == 0 || j == 0 || j == maxC - 1)  // PARETS
                    taulell[i][j] = 1;                  // paret
                else if (                               // PECES DE COLORS
                        i >= 4                          //
                                &&
                                i <= 11
                                &&
                                j >= i - 4 && j <= maxC - 1 + 4 - i
                )
                    taulell[i][j] = 2;              // peça de color
                else
                    taulell[i][j] = 0;              // espai en blanc

                if (i == maxF - 1) {                // FORATS EN LA PART DE BAIX (PISO)
                    if (contadorTemps % 10 == 0)
                        if (j % 3 != 0)
                            taulell[i][j] = 1;
                        else
                            taulell[i][j] = 0;
                }
            }
        }


        // posicionem la base al mig (abans de jugar)
        taulell[maxF - 2][maxC / 2] = 3;            // base nau
        taulell[maxF - 3][maxC / 2] = -1;           // pilota


        //</editor-fold>


        //<editor-fold desc="Sortida Aleatòria">
        if (rnd.nextBoolean())
            direccioPilota = 9;
        else
            direccioPilota = 7;
        //</editor-fold>


        //<editor-fold desc="Posem els fantasmes a l'inici">
        // inicialitzem els 4 fantasmes. fila, columna i direcció
        int filaF, colF, dirF;
        for (int i = 0; i < fantasmes.length; i++) {
            fantasmes[i][0] = 2;                        // posició I (fila)
            fantasmes[i][1] = maxC / 2;                 // posició J (col )
            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)]; // direcció (1,2,3,4,5,6,7,8)
        }
        //</editor-fold>


        // COMENCEM EL JOC!!!
        do {
            if (pausa) {            // si premem la p pausarem el joc
                Thread.sleep(100);
            }else {                 // si premem la r el despausem


                //<editor-fold desc="Movem els fantasmes">
                for (int i = 0; i < fantasmes.length; i++) {

                    filaF = fantasmes[i][0];      // fila
                    colF = fantasmes[i][1];       // col
                    dirF = fantasmes[i][2];       // dir

                    if (dirF == 6) {
                        if (taulell[filaF][colF + 1] == 0) {      // dreta
                            colF++;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 8) {
                        if (taulell[filaF - 1][colF] == 0) {      // amunt
                            filaF--;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 4) {
                        if (taulell[filaF][colF - 1] == 0) {      // esq
                            colF--;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 2) {
                        if (taulell[filaF + 1][colF] == 0 && filaF + 2 <= maxF - 1) {      // baix
                            filaF++;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 1) {
                        if (taulell[filaF + 1][colF - 1] == 0 && filaF + 2 <= maxF - 1) {      // baix-esquerra
                            filaF++;
                            colF--;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 3) {
                        if (taulell[filaF + 1][colF + 1] == 0 && filaF + 2 <= maxF - 1) {      // baix-dreta
                            filaF++;
                            colF++;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 9) {
                        if (taulell[filaF - 1][colF + 1] == 0) {      // dalt-dreta
                            filaF--;
                            colF++;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    } else if (dirF == 7) {
                        if (taulell[filaF - 1][colF - 1] == 0) {      // dalt-esquerra
                            filaF--;
                            colF--;
                            fantasmes[i][0] = filaF;
                            fantasmes[i][1] = colF;
                        } else {
                            fantasmes[i][2] = dirsFantasmes[rnd.nextInt(dirsFantasmes.length)];
                        }
                    }
                }
                //</editor-fold>


                //<editor-fold desc="Movem la pilota">
                //           // MOVEM LA PILOTA !!!
                switch (direccioPilota) {       // (direcció abans de rebotar)
                    case 9:
                        if (taulell[filaPilota - 1][colPilota + 1] != 1) {      // si la posició està buida
                            if (taulell[filaPilota - 1][colPilota + 1] == 2) {  // si la pilota trenca una peça
                                punt = true;
                                taulell[filaPilota - 1][colPilota + 1] = 0;
                                if (colPilota >= maxC - 2) {
                                    direccioPilota = 7;                         // rebotem
                                } else
                                    direccioPilota = 3;
                            } else {
                                taulell[filaPilota][colPilota] = 0;             // eliminem la pilota del lloc d'abans
                                filaPilota = filaPilota - 1;
                                colPilota = colPilota + 1;
                                taulell[filaPilota][colPilota] = -1;            // pugem cap amunt
                            }

                        } else if (colPilota >= maxC - 2) {
                            direccioPilota = 7;                                 // rebotem
                        } else
                            direccioPilota = 3;
                        break;
                    case 7:
                        if (taulell[filaPilota - 1][colPilota - 1] != 1) {      // si la posició està buida
                            if (taulell[filaPilota - 1][colPilota - 1] == 2) {  // si la pilota trenca una peça
                                punt = true;
                                taulell[filaPilota - 1][colPilota - 1] = 0;
                                if (colPilota <= 1) {
                                    direccioPilota = 9;                         // rebotem
                                } else
                                    direccioPilota = 1;
                            } else {
                                taulell[filaPilota][colPilota] = 0;             // eliminem la pilota del lloc d'abans
                                filaPilota = filaPilota - 1;
                                colPilota = colPilota - 1;
                                taulell[filaPilota][colPilota] = -1;            // pugem cap amunt
                            }
                        } else if (colPilota <= 1) {
                            direccioPilota = 9;                                 // rebotem
                        } else
                            direccioPilota = 1;
                        break;
                    case 1:
                        if (taulell[filaPilota + 1][colPilota - 1] != 1) {      // si la posició està buida
                            if (taulell[filaPilota + 1][colPilota - 1] == 2) {  // si la pilota trenca una peça
                                punt = true;
                                taulell[filaPilota + 1][colPilota - 1] = 0;
                                if (colPilota <= 1) {
                                    direccioPilota = 3;                         // rebotem
                                } else
                                    direccioPilota = 7;
                            } else {
                                taulell[filaPilota][colPilota] = 0;             // eliminem la pilota del lloc d'abans
                                filaPilota = filaPilota + 1;
                                colPilota = colPilota - 1;
                                taulell[filaPilota][colPilota] = -1;            // pugem cap amunt
                            }
                        } else if (colPilota <= 1) {
                            direccioPilota = 3;                                 // rebotem
                        } else
                            direccioPilota = 7;
                        break;
                    case 3:
                        if (taulell[filaPilota + 1][colPilota + 1] != 1) {      // si la posició està buida
                            if (taulell[filaPilota + 1][colPilota + 1] == 2) {  // si la pilota trenca una peça
                                punt = true;
                                taulell[filaPilota + 1][colPilota + 1] = 0;
                                if (colPilota >= maxC - 2) {
                                    direccioPilota = 1;                         // rebotem
                                } else
                                    direccioPilota = 9;
                            } else {
                                taulell[filaPilota][colPilota] = 0;             // eliminem la pilota del lloc d'abans
                                filaPilota = filaPilota + 1;
                                colPilota = colPilota + 1;
                                taulell[filaPilota][colPilota] = -1;            // pugem cap amunt
                            }
                        } else if (colPilota >= maxC - 2) {
                            direccioPilota = 1;                                 // rebotem
                        } else
                            direccioPilota = 9;
                        break;
                }
                if (punt) {
                    puntsTotals += 10;
                    punt = false;
                }

                //</editor-fold>


                //<editor-fold desc="Imprimim el taulell">
                // imprimim el taulell
                System.out.println(cafeN + "\n\n\n\n\n\t\t\t      A R K A N O I D\t\t" + blauC + puntsTotals);
                System.out.println(cafeN + "\t\t\t      - - - - - - - -" + nc);
                for (int i = 0; i < maxF; i++) {
                    System.out.print("\t\t");
                    for (int j = 0; j < maxC; j++) {

                        // primer recorrem totes les posicions dels fantasmes
                        // per si coicideix alguna
                        boolean hiHaFantasma = false;
                        for (int k = 0; k < maxFantasmes; k++) {
                            if (i == fantasmes[k][0] && j == fantasmes[k][1])
                                hiHaFantasma = true;
                        }
                        // si hi ha algun fantasma imprmimi el fantasma
                        if (hiHaFantasma)
                            System.out.print(purpuraN + " " + fantasma + " " + nc);   // imprimim fantasma //
                        else if (taulell[i][j] == 0) {                  // sinó imprimim espai en blanc
                            System.out.print("   ");
                        } else if (taulell[i][j] == -1) {               // sinó imprmimim la bola
                            System.out.print(" * ");
                        } else if (taulell[i][j] == 1) {                // sinó imprimim la base ===
                            if (i == maxF - 2 && j > 0 && j < maxC - 1) // (no és la penúltima fila ni la 1a ni última col)
                                System.out.print(cafeN + "===" + nc);
                            else if (i == maxF - 1)                     // sinó imprimim la paret de baix
                                System.out.print(roigN + "###" + nc);   // (pis de baix)
                            else
                                System.out.print(verdN + "#  " + nc);   // i les parets laterals i superior
                        } else if (taulell[i][j] == 2) {                // sinó imprimim les peces de colors
                            if (i % 2 == 0)
                                System.out.print(cafeN + "€€ " + nc);
                            else
                                System.out.print(blauC + "$$ " + nc);
                        }
                    }
                    System.out.println();
                }
                System.out.println(verdN+"\n\t\t\tP) Pausar\tR) Reanudar"+nc);
                System.out.println("\n");
                //</editor-fold>


                //<editor-fold desc="Velocitat joc">
                if (contadorTemps % 25 == 0 && velocitat > 100)
                    velocitat = velocitat - 2;
                Thread.sleep(velocitat);
                contadorTemps++;
                //</editor-fold>


                //<editor-fold desc="Movem les peces de color cap avall">
                // movem totes les peces de colors una fila avall
                if (contadorTemps % 100 == 0) {
                    for (int i = maxF - 4; i > 0; i--) {
                        for (int j = 0; j < maxC; j++) {
                            if (taulell[i][j] == 2) {
                                taulell[i + 1][j] = taulell[i][j];
                                taulell[i][j] = 0;
                                //taulell[i+1][j] = taulell[i][j];
                            }
                        }
                    }
                }
                //</editor-fold>


                //<editor-fold desc="Contador de peces i final">
                // contem les peces
                contaPeces = 0;
                for (int i = 0; i < maxF; i++) {
                    for (int j = 0; j < maxC; j++) {
                        if (taulell[i][j] == 2)
                            contaPeces++;
                    }
                }

                if (filaPilota >= maxF - 1 || contaPeces <= 0)
                    fiJoc = true;
                //</editor-fold>
            }// fi pausa
        }while (!fiJoc);

        //<editor-fold desc="Informació quan acaba el joc">
        if (contaPeces <= 0) {
            System.out.println(verdN + "\n\n\n \t\tENHORABONA. HAS GUANYAT!!\n\n");
        } else
            System.out.println(roigN + "\t\tHAS PERDUT!!!!\n\n" + nc);

        System.out.println(blauC + "\t\tHAS FET " + puntsTotals + " PUNTS" + nc);
        System.out.println("\n\n");
        //</editor-fold>





    }
}