# Simple Arcanoid Estructurat

## Per practicar matrius

- És el típic joc d'arkanoid però fet amb matrius i caràcters.
- Hi haurà una matriu de nombres sencers que serà el taulell de joc
- El programa sols té la classe Main i mètode main(). 
- **No té cap** altra **classe** extra, **mètode** extra, **funció** ni **llibreria** externa


- Els elements del joc es poden codificar així, però podeu utilitzar la codificació que vullgau:

            ▪ pilota:		núm -1	→   *
            ▪ espai blanc:	núm 0	→  
            ▪ paret: 		núm 1	→    # 
            ▪ peça color: 	núm 2	→  $,€
            ▪ base movible: 	núm 3	→    ===
            ▪ fantasma: 	núm 8	→    &


![arcanoid.png](images%2Farcanoid.png)

El taulell (matriu) serà una matrius de números sencers i ha de quedar així:

![taulell.png](images%2Ftaulell.png)

### **Aspectes tècnics**:

- El codi s'ha creat amb IntellIJ i java 11
- Per executar cal crear l'artefacte jar i executar-lo en consola.
- Pots generar l'arxiu executable des de `Gradle > Distribution > installDist` 
- En la carpeta `build > install > Arcanoid` estarà el jar 
- EN LINUX FUNCIONA OK. EN WINDOWS S'HAURIA D'INSTAL·LAR LA CONSOLA GIT I EXECUTAR-HO DES D'AHI

### **Execució**:
- Si has creat l'executable des de Gradle amb: `Distribution > installDist`, has d'anar a la consola i executar-lo com qualsevol fitxer executable.
- ...
- Si has generat el jar amb Build Project, per executar-lo ho farem amb aquesta ordre: `java -jar arcanoid.jar`
- La base de l'arcanoid es mou pel taulell cap a la dreta i esquerra
- Podem moure la base a partir de les tecles 4(esquerra), 6(dreta)
- Els fantasmes van aleatòriament pel taulell sense travessar parets. Són una distracció visual però no fan altra cosa
- La velocitat s'incrementa paulatinament, poc a poc
- Les peces van baixant paulatinament en bloc cap avall.
- El joc acaba quan la pilota trenca totes les peces o quan acaba dintre d'un forat

### **VÍDEO**:
Una petita demostració del funcionament del joc:

[Arcanoid amb caràcters](https://drive.google.com/file/d/1DpILwpF7iwt9C1agsMN4yod2PmT9YaTi/view?usp=sharing)


## Contribució
Si vols contribuir, si us plau, fes un *fork* del repositori i envia una sol·licitud de *pull request* amb les teves millores.

## Llicència
Aquest projecte està llicenciat sota la [Llicència MIT](LICENSE).
